import moment from "moment";

export const dateFormatter = (label: any) => moment(label).format("HH:mm:ss DD MMM YYYY");