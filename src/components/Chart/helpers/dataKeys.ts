type DataKeysProps = {
  dataKey: string| number;
  color: string;
}

export const dataKeys:DataKeysProps[] = [
    {dataKey: "flareTemp", color: "#8884d8"},
    {dataKey:"injValveOpen", color: "#006400" },
    {dataKey: "oilTemp", color: "#ff8c00"},
    {dataKey: "casingPressure", color: "#ff0000"},
    {dataKey: "tubingPressure", color: "#ff00ff" },
    {dataKey: "waterTemp", color: "#00ffff"}
];

