
export const formatTooltip = (value: number | string | undefined, name: number | string | undefined) => {
  if(name === "flareTemp" || name === "oilTemp" || name === "waterTemp"){
    return [`${value} F`, `${name}`];
  }

  if(name === "casingPressure" || name === "tubingPressure"){
    return [`${value} PSI`, `${name}`];
  }

  if(name === "injValveOpen"){
    return [`${value} %`, `${name}`];
  }
}