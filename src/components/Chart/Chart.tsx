import React, { FC } from 'react';
import {
  AreaChart,
  Area,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  ResponsiveContainer,
  LineChart,
  Line,
  Legend,
} from 'recharts';
import { formatTooltip } from './helpers/formatTooltip';
import { dataKeys } from './helpers/dataKeys';
import { dateFormatter } from './helpers/dateFormatter';
import moment from 'moment';

export interface DataProps {
  date: number | undefined | string;
  flareTemp?: number;
  injValveOpen?: number;
  oilTemp?: number;
  casingPressure?: number;
  tubingPressure?: number;
  waterTemp?: number;
}

export interface ChartProps {
  height?: number | string;
  maxHeight?: number;
  minHeight?: number;
  width?: number | string;
  minWidth?: number | string;
  data: Partial<DataProps>[];
  marginTop?: number;
  marginRight?: number;
  marginLeft?: number;
  marginBottom?: number;
  chartStyle?: 'line' | 'area';
  areaType?:
    | 'basis'
    | 'basisClosed'
    | 'basisOpen'
    | 'linear'
    | 'linearClosed'
    | 'natural'
    | 'monotoneY'
    | 'monotone'
    | 'step'
    | 'stepBefore'
    | 'stepAfter'
    | Function;
  cartesianGrid?: boolean;
  addLegend?: boolean;
}

const Chart: FC<ChartProps> = ({
  height,
  maxHeight,
  width,
  minWidth,
  minHeight,
  data,
  marginBottom = 0,
  marginLeft = 0,
  marginRight = 30,
  marginTop = 10,
  chartStyle = 'line',
  areaType = 'linear',
  cartesianGrid = false,
  addLegend = false
}) => {
  const StyleAreaChart = (
    <AreaChart
      data={data}
      margin={{
        top: marginTop,
        right: marginRight,
        left: marginLeft,
        bottom: marginBottom,
      }}
    >
      {cartesianGrid ? <CartesianGrid strokeDasharray="3 3" /> : null}
      <XAxis
        dataKey="date"
        domain={['auto', 'auto']}
        name="date"
        tickCount={10}
        tickFormatter={unixTime => moment(unixTime).format('HH:mm')}
        type="number"
      />
      <YAxis />
      <Tooltip
        labelFormatter={dateFormatter}
        formatter={(value: any, name: any, props: any) => {
          return formatTooltip(value, name);
        }}
      />
      {addLegend? <Legend />: null}
      {dataKeys.map((item, index) => (
        <Area key={index} type={areaType} dataKey={item.dataKey} stackId="1" stroke={item.color} fill={item.color} />
      ))}
    </AreaChart>
  );

  const StyleLineChart = (
    <LineChart
      data={data}
      margin={{
        top: marginTop,
        right: marginRight,
        left: marginLeft,
        bottom: marginBottom,
      }}
    >
      {cartesianGrid ? <CartesianGrid strokeDasharray="3 3" /> : null}
      <XAxis
        dataKey="date"
        domain={['auto', 'auto']}
        name="date"
        tickCount={10}
        tickFormatter={unixTime => moment(unixTime).format('HH:mm')}
        type="number"
      />
      <YAxis />
      <Tooltip
        labelFormatter={dateFormatter}
        formatter={(value: any, name: any, props: any) => {
          return formatTooltip(value, name);
        }}
      />
      {addLegend?<Legend />:null}
      {dataKeys.map((item, index) => (
        <Line key={index} type={areaType} dataKey={item.dataKey} stroke={item.color} dot={false} />
      ))}
    </LineChart>
  );

  return (
    <ResponsiveContainer height={height} width={width} maxHeight={maxHeight} minHeight={minHeight} minWidth={minWidth}>
      {chartStyle === 'line' ? StyleLineChart : StyleAreaChart}
    </ResponsiveContainer>
  );
};

export default Chart;
