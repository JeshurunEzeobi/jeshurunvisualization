import { gql } from "@apollo/client";

export const GET_METRICS = gql`
  query getMetrics {
    getMetrics
  }
`;

export const GET_MEASUREMENTS = gql`
  query getMeasurements($input: MeasurementQuery!) {
    getMeasurements(input: $input) {
      metric
      at
      value
      unit
    }
  }
`;

export const GET_LAST_KNOWN_MEASUREMENT = gql`
  query getLastKnownMeasurement($metricName: String!){
    getLastKnownMeasurement(metricName: $metricName){
      metric
      at
      value
      unit
    }
  }
`;

export const GET_MULTIPLE_MEASUREMENTS =gql`
  query getMultipleMeasurements($input: [MeasurementQuery]!) {
    getMultipleMeasurements(input: $input) {
      metric
      measurements {
        metric
        at
        value
        unit
      }
    }
  }
`;

export const NEW_MEASUREMENT_SUBSCRIPTION =gql`
  subscription newMeasurement {
    newMeasurement {
      metric
      at
      value
      unit
    }
  }
`;
