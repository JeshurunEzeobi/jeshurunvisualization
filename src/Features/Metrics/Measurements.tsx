import React, { FC, useEffect, useMemo, useState } from 'react';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import ReactLoading from 'react-loading';
import { makeStyles } from '@material-ui/core/styles';
import { selectOptions } from './helpers/selectOptions';
import Select from 'react-select';
import makeAnimated from 'react-select/animated';
import Typography from '@material-ui/core/Typography';
import { IState } from '../../redux/store';
import { useDispatch, useSelector } from 'react-redux';
import { getMetrics, getMultiMeasurements, getMetricError } from '../../redux/actions/Metrics/Metrics';
import { metricsList } from './helpers/metricsList';
import { GET_MULTIPLE_MEASUREMENTS } from './Queries';
import { inputMultiple } from './helpers/inputMultiple';
import { useLazyQuery } from '@apollo/client';
import moment from 'moment';
import Chart from '../../components/Chart/Chart';
import { chartDataMulipleMeasureMents } from './helpers/chartDataMultipleMeasurements';

const animatedComponents = makeAnimated();
const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    marginBottom: theme.spacing(3),
  },
  container: {
    marginTop: theme.spacing(10),
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  typography: {
    fontSize: theme.spacing(3),
    fontWeight: 700,
    color: '#000080',
  },
  selectContainer: {
    width: theme.spacing(40),
    marginTop: theme.spacing(3),
  },
  loading: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    minHeight: theme.spacing(40),
  },
  card: {
    boxShadow: 'rgb(40, 78, 123) 0px 0px 30px -15px',
    width: '80%',
    height: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: theme.spacing(0.5),
    minHeight: theme.spacing(40),
  },
  circularProgress : {
    marginTop: theme.spacing(5),
    marginBottom: theme.spacing(5),
  }
}));

const selectorMetrics = (state: IState) => {
  const { metrics, multiMeasurements } = state.metrics;
  return {
    metrics,
    multiMeasurements,
  };
};

const Measurements: FC = () => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const [inputQuery, setInputQuery] = useState([{ metricName: '' }]);
  const { metrics, multiMeasurements } = useSelector(selectorMetrics);
  const after: number = useMemo(() => {
    return moment()
      .subtract(1, 'hour')
      .valueOf();
  }, []);
  const before: number = useMemo(() => {
    return Date.now();
  }, []);
  const inputVariable = useMemo(() => {
    return inputMultiple(metrics, before, after);
  }, [metrics,after,before]);

  useEffect(() => {
    if (metrics.length !== 0 && inputVariable[0].metricName !== '') {
      setInputQuery(inputVariable);
    }
  }, [metrics, inputVariable]);

  const [getMultipleMeasurements, { data, error, loading }] = useLazyQuery(GET_MULTIPLE_MEASUREMENTS);

  useEffect(() => {
    if (inputQuery[0].metricName !== '') {
      getMultipleMeasurements({ variables: { input: inputQuery } });
    }
  }, [inputQuery,getMultipleMeasurements]);

  useEffect(() => {
    if (data && !error) {
      dispatch(getMultiMeasurements(data.getMultipleMeasurements));
    }
    if (error) {
      dispatch(getMetricError(error));
      return;
    }
  }, [dispatch, data, error]);

  const handleOnselectChange = (values: any) => {
    if (values !== null && values !== undefined) {
      dispatch(getMetrics(metricsList(values)));
    } else {
      dispatch(getMultiMeasurements([]));
      dispatch(getMetrics([]));
      setInputQuery([{ metricName: '' }]);
    }
  };

  if (error && multiMeasurements.length === 0)
    return (
      <div className={classes.loading}>
        <Card className={classes.card}>
          <h5>...something went wrong</h5>
        </Card>
      </div>
    );

  if (loading && multiMeasurements.length === 0)
    return (
      <div className={classes.loading}>
        <Card className={classes.card}>
          <ReactLoading type="bars" color="#73C2FB" />
        </Card>
      </div>
    );

  return (
    <Grid container classes={{ root: classes.root }}>
      <Grid item xs={12} sm={12}></Grid>
      <Grid item xs={12} sm={12}>
        <div className={classes.container}>
          <Typography classes={{ root: classes.typography }}>Select your Metrics and view Measurements</Typography>
            {loading? <div className={classes.circularProgress}><ReactLoading type="spinningBubbles" color="#73C2FB" /></div>:null}
          <div className={classes.selectContainer}>
            <Select
              placeholder="Select...Metrics"
              closeMenuOnSelect={false}
              components={animatedComponents}
              isMulti
              options={selectOptions}
              onChange={handleOnselectChange}
            />
          </div>
          {metrics.length > 0 && multiMeasurements.length > 0 ? (
            <Chart
              data={chartDataMulipleMeasureMents(multiMeasurements)}
              height={450}
              areaType="monotone"
              chartStyle="area"
              cartesianGrid={false}
              addLegend={true}
            />
          ) : null}
          {metrics.length > 0 && multiMeasurements.length > 0 ? (
            <Chart
              data={chartDataMulipleMeasureMents(multiMeasurements)}
              height={450}
              areaType="monotone"
              chartStyle="line"
              cartesianGrid={false}
              addLegend={true}
            />
          ) : null}
        </div>
      </Grid>
      <Grid item xs={12} sm={12}></Grid>
    </Grid>
  );
};

export default Measurements;
