import { Measurement } from "./chartDataMultipleMeasurements";
import { DataProps } from "../../../components/Chart/Chart"

export const roundUp =(input: number| undefined): number | undefined => {
  if(input === undefined) {
    return undefined;
  }else {
    return Math.ceil(input);
  }
}

export const chartDataGetMeasurements = (data: Measurement[]): Partial<DataProps>[] => {
    const dataLength = data.length;
    const currentMetric = data[0].metric;
   

    let result: Partial<DataProps>[]=[{date: 0}];

    if (currentMetric === 'flareTemp') {
        for (let k = 0; k < dataLength; k++) {
          result[k] ={date: data[k].at, flareTemp:  roundUp(data[k].value)};
        }

        return result;
      }
  
      if (currentMetric === 'injValveOpen') {
        for (let k = 0; k < dataLength; k++) {
            result[k] = {date: data[k].at, injValveOpen: roundUp(data[k].value)};
          }
  
          return result;
      }
  
      if (currentMetric === 'oilTemp') {
        for (let k = 0; k < dataLength; k++) {
            result[k]= {date: data[k].at, oilTemp: roundUp(data[k].value)};
          }
  
          return result;
      }
  
      if (currentMetric === 'casingPressure') {
        for (let k = 0; k < dataLength; k++) {
            result[k] ={date: data[k].at, casingPressure: roundUp(data[k].value)};
          }
  
          return result;
      }
  
      if (currentMetric === 'tubingPressure') {
        for (let k = 0; k < dataLength; k++) {
            result[k] = {date: data[k].at, tubingPressure: roundUp(data[k].value)};
          }
  
          return result;
      }
  
      if (currentMetric === 'waterTemp') {
        for (let k = 0; k < dataLength; k++) {
            result[k]={date: data[k].at, waterTemp: roundUp(data[k].value)};
          }
  
          return result;
      }

      return result;

      
}