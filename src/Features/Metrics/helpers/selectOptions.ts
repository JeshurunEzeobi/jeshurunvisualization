export const selectOptions = [
  { label: 'flareTemp', value: 'flareTemp' },
  { label: 'injValveOpen', value: 'injValveOpen' },
  { label: 'oilTemp', value: 'oilTemp' },
  { label: 'casingPressure', value: 'casingPressure' },
  { label: 'tubingPressure', value: 'tubingPressure' },
  { label: 'waterTemp', value: 'waterTemp' },
];
