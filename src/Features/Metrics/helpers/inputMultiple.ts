export const inputMultiple = (data: string[], before: number, after: number) => {
    let input= [{metricName: "", before: 0, after: 0}]
    if(data !== null || data !== undefined) {
        if(data.length > 0) {
           input =  data.map((item: string) => {
              return {metricName: item, after, before}
            });
            return input;
        }
    }

   return input;
}