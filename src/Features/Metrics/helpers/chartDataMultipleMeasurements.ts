import { roundUp } from "./chartDataGetMeasurements";
import { DataProps } from "../../../components/Chart/Chart";

export interface Measurement {
  metric?: string;
  at?: number | undefined;
  value?: number | undefined;
  unit?: string;
}

export interface MultipleMeasurement {
  metric: string;
  measurements: Measurement[];
}



export const chartDataMulipleMeasureMents = (queryData: MultipleMeasurement[]): Partial<DataProps>[] => {
  const dataLength = queryData.length;
  const measurementsLength = queryData[0].measurements.length;
  let initialResult: Partial<DataProps>[] = [];
  let finalResult: Partial<DataProps>[] =[];
  const dates = [];
  const flareTemps = [];
  const injValveOpens = [];
  const oilTemps = [];
  const casingPressures = [];
  const tubingPressures = [];
  const waterTemps = [];

  for (let i = 0; i < measurementsLength; i++) {
    dates.push(queryData[0].measurements[i].at);
  }


  for (let i = 0; i < dataLength; i++) {
    const currentMetric = queryData[i].metric;

    if (currentMetric === 'flareTemp') {
      for (let k = 0; k < measurementsLength; k++) {
        flareTemps.push(queryData[i].measurements[k].value);
      }
    }

    if (currentMetric === 'injValveOpen') {
      for (let k = 0; k < measurementsLength; k++) {
        injValveOpens.push(queryData[i].measurements[k].value);
      }
    }

    if (currentMetric === 'oilTemp') {
      for (let k = 0; k < measurementsLength; k++) {
        oilTemps.push(queryData[i].measurements[k].value);
      }
    }

    if (currentMetric === 'casingPressure') {
      for (let k = 0; k < measurementsLength; k++) {
        casingPressures.push(queryData[i].measurements[k].value);
      }
    }

    if (currentMetric === 'tubingPressure') {
      for (let k = 0; k < measurementsLength; k++) {
        tubingPressures.push(queryData[i].measurements[k].value);
      }
    }

    if (currentMetric === 'waterTemp') {
      for (let k = 0; k < measurementsLength; k++) {
        waterTemps.push(queryData[i].measurements[k].value);
      }
    }
  }

  for(let p=0; p < measurementsLength; p++){
     initialResult.push({date: dates[p]});
  }

  if(waterTemps.length > 0){
    for(let q = 0; q < measurementsLength; q++){
      finalResult[q] ={...initialResult[q], waterTemp: roundUp(waterTemps[q])};
    }
    initialResult = finalResult;
  }

  if(tubingPressures.length > 0){
    for(let q = 0; q < measurementsLength; q++){
      finalResult[q]= {...initialResult[q], tubingPressure: roundUp(tubingPressures[q])};
    }
    initialResult = finalResult;
  }

  if(flareTemps.length > 0){
    for(let q = 0; q < measurementsLength; q++){
      finalResult[q] = {...initialResult[q], flareTemp: roundUp(flareTemps[q])};
    }
    initialResult = finalResult;
  }

  if(injValveOpens.length > 0){
    for(let q = 0; q < measurementsLength; q++){
      finalResult[q] = {...initialResult[q], injValveOpen: roundUp(injValveOpens[q])};
    }
    initialResult = finalResult;
  }


  if(oilTemps.length > 0){
    for(let q = 0; q < measurementsLength; q++){
      finalResult[q] = {...initialResult[q], oilTemp: roundUp(oilTemps[q])};
    }
    initialResult = finalResult;
  }

  if(casingPressures.length > 0){
    for(let q = 0; q < measurementsLength; q++){
      finalResult[q] = {...initialResult[q], casingPressure: roundUp(casingPressures[q])};
    }
    initialResult = finalResult;
  }
  
  return finalResult;
  
};
