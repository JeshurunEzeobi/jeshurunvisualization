export interface selectData {
    value: string;
    label: string;
}
export const metricsList = (input: selectData[]): string[] => {
    const result = input.map((item)=>{
        return `${item.value}`;
    })
    return result;
}