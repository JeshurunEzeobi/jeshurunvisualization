import React, { FC } from 'react';
import { IState } from '../../redux/store';
import { useSelector } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import MeasurementCard from './MeasurementCard';
import Measurements from './Measurements';
import MeasurementTile from './MeasurementTile';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  grid: {
      marginTop: theme.spacing(3),
  }
}));

const selectorMetrics = (state: IState) => {
  const { metrics } = state.metrics;
  return {
    metrics,
  };
};

const Metrics: FC = () => {
  const classes = useStyles();
  const { metrics } = useSelector(selectorMetrics);

  return (
    <div className={classes.root}>
      <Grid classes={{root: classes.grid}} container>
        {metrics.length > 0 ? metrics.map((item, index) => <MeasurementTile key={index} metricName={item} />) : null}
      </Grid>
      <Measurements />
      {metrics.length > 0 ? metrics.map((item, index) => <MeasurementCard key={index} metricName={item} />) : null}
    </div>
  );
};

export default Metrics;
