import React, { FC, useEffect, Fragment, useMemo } from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { makeStyles } from '@material-ui/core/styles';
import { GET_MEASUREMENTS } from './Queries';
import Chart from '../../components/Chart/Chart';
import { useSelector, useDispatch } from 'react-redux';
import { getMetricError, getMeasurements } from '../../redux/actions/Metrics/Metrics';
import { chartDataGetMeasurements } from './helpers/chartDataGetMeasurements';
import moment from 'moment';
import { useQuery } from '@apollo/client';
import { IState } from '../../redux/store';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    minHeight: theme.spacing(40),
  },
  card: {
    boxShadow: 'rgb(40, 78, 123) 0px 0px 30px -15px',
    width: '80%',
    height: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: theme.spacing(0.5),
    minHeight: theme.spacing(40),
  },
  cardContent: {
    boxSizing: 'border-box',
    width: '100%',
    height: '100%',
    borderRadius: theme.spacing(0.5),
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    background: 'white',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    width: '100%',
    color: '#000080',
    textAlign: 'center',
  },
}));

export interface MeasurementCardProps {
  metricName: string;
}

const selectorMetrics = (state: IState) => {
  const { metrics, flareTemps, injValveOpens, oilTemps, casingPressures, tubingPressures, waterTemps } = state.metrics;
  return {
    metrics,
    flareTemps,
    injValveOpens,
    oilTemps,
    casingPressures,
    tubingPressures,
    waterTemps,
  };
};

const MeasurementCard: FC<MeasurementCardProps> = ({ metricName }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { metrics, flareTemps, injValveOpens, oilTemps, casingPressures, tubingPressures, waterTemps } = useSelector(
    selectorMetrics,
  );
  const after: number = useMemo(() => {
    return moment()
      .subtract(1, 'hour')
      .valueOf();
  }, []);
  const before: number = useMemo(() => {
    return Date.now();
  }, []);
  const input = { metricName, after, before };

  const { data, error } = useQuery(GET_MEASUREMENTS, { variables: { input } });

  useEffect(() => {
    if (error) {
      dispatch(getMetricError(error));
      return;
    }
    if (data) {
      dispatch(getMeasurements(data.getMeasurements, metricName));
    }
  }, [dispatch, data, error, metricName]);

  const displayChart = (data: any) => (
    <Chart
      data={chartDataGetMeasurements(data)}
      height={450}
      areaType="monotone"
      chartStyle="area"
      cartesianGrid={false}
    />
  );

  return (
    <Fragment>
      {metricName.length > 0 && metrics.length > 0 ? (
        <div className={classes.root}>
          <Card classes={{ root: classes.card }}>
            <CardContent classes={{ root: classes.cardContent }}>
              <h1 className={classes.title}>Measurements for the last hour</h1>
               {metricName === "flareTemp" && flareTemps.length > 0? displayChart(flareTemps): null}
               {metricName === "injValveOpen" && injValveOpens.length > 0? displayChart(injValveOpens): null}
               {metricName === "oilTemp" && oilTemps.length> 0 ? displayChart(oilTemps): null}
               {metricName === "casingPressure" && casingPressures.length > 0? displayChart(casingPressures): null}
               {metricName === "tubingPressure" && tubingPressures.length > 0? displayChart(tubingPressures): null}
               {metricName === "waterTemp" && waterTemps.length > 0? displayChart(waterTemps): null}
              <h1 className={classes.title}>{metricName}</h1>
            </CardContent>
          </Card>
        </div>
      ) : null}
    </Fragment>
  );
};

export default MeasurementCard;
