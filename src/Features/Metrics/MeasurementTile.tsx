import React, { FC, useEffect, Fragment } from 'react';
import Grid from '@material-ui/core/Grid';
import { useSelector, useDispatch } from 'react-redux';
import { MeasurementCardProps } from './MeasurementCard';
import { IState } from '../../redux/store';
import { makeStyles } from '@material-ui/core/styles';
import { useQuery } from '@apollo/client';
import Card from '@material-ui/core/Card';
import { getMetricError, getLastMeasurement } from '../../redux/actions/Metrics/Metrics';
import { GET_LAST_KNOWN_MEASUREMENT } from './Queries';

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    marginTop: theme.spacing(2),
  },  
  card: {
    width: '90%',
    height: theme.spacing(15),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  title: {
    color: '#000080',
    fontSize: theme.spacing(3)
  },
  body: {
    fontSize: theme.spacing(3),
  }
}));

export interface MeasurementTileProps {
  matricName: String;
}

const selectorMetrics = (state: IState) => {
  
  const { metrics, flareTemp, injValveOpen, oilTemp, casingPressure, tubingPressure, waterTemp } = state.metrics;
  return {
    metrics,
    flareTemp,
    injValveOpen,
    oilTemp,
    casingPressure,
    tubingPressure,
    waterTemp,
  };
};

const MeasurementTile: FC<MeasurementCardProps> = ({ metricName }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { flareTemp, injValveOpen, oilTemp, casingPressure, tubingPressure, waterTemp } = useSelector(
    selectorMetrics,
  );
  const { data, error } = useQuery(GET_LAST_KNOWN_MEASUREMENT, {
    variables: { metricName },
    pollInterval: 1000,
  });

  useEffect(() => {
    if (error) {
      dispatch(getMetricError(error));
      return;
    }
    if (data) {
      dispatch(getLastMeasurement(data.getLastKnownMeasurement, metricName));
    }
  }, [dispatch, data, error, metricName]);

  const displayTile = (data: any) => (
    <Grid classes={{root: classes.root}} xs={12} sm={6} md={4}>
      <Card classes={{ root: classes.card }}>
        <h3 className={classes.title}>{metricName}</h3>
        <span className={classes.body}>{`${data.value} ${data.unit}`}</span>
      </Card>
    </Grid>
  );

  return (
    <Fragment>
      {metricName === 'flareTemp' && Object.keys(flareTemp).length > 0 ? displayTile(flareTemp) : null}
      {metricName === 'injValveOpen' && Object.keys(injValveOpen).length > 0 ? displayTile(injValveOpen) : null}
      {metricName === 'oilTemp' && Object.keys(oilTemp).length > 0 ? displayTile(oilTemp) : null}
      {metricName === 'casingPressure' && Object.keys(casingPressure).length > 0 ? displayTile(casingPressure) : null}
      {metricName === 'tubingPressure' && Object.keys(tubingPressure).length > 0 ? displayTile(tubingPressure) : null}
      {metricName === 'waterTemp' && Object.keys(waterTemp).length > 0 ? displayTile(waterTemp) : null}
    </Fragment>
  );
};

export default MeasurementTile;
