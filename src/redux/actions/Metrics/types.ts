import {
  METRICS_ERROR,
  GET_METRICS,
  GET_MEASUREMENTS,
  GET_LAST_MEASUREMENT,
  GET_MULTIPLE_MEASUREMENTS,
} from '../../../constants/actionTypes';
import { ApolloError } from '@apollo/client';

export interface Measurement {
  metric: string;
  at: number;
  value: number;
  unit: string;
}

export interface MultiMeasurements {
  metric: string;
  measurements: Measurement[];
}
export interface MetricsState {
  flareTemps: Measurement[];
  injValveOpens: Measurement[];
  oilTemps: Measurement[];
  casingPressures: Measurement[];
  tubingPressures: Measurement[];
  waterTemps: Measurement[];
  multiMeasurements: MultiMeasurements[];
  metrics: string[];
  flareTemp: Measurement | {};
  injValveOpen: Measurement| {};
  oilTemp: Measurement| {};
  casingPressure: Measurement| {};
  tubingPressure: Measurement| {};
  waterTemp: Measurement| {};
  metricErrors: ApolloError | undefined;
}

export interface GetMetricsAction {
  type: typeof GET_METRICS;
  payload: string[];
}

export interface GetMeasurementsAction {
  type: typeof GET_MEASUREMENTS;
  payload: {
    measurements: Measurement[];
    metricType: string;
  };
}

export interface MetricsErrorAction {
  type: typeof METRICS_ERROR;
  payload: ApolloError;
}

export interface GetMultipleMeasurementsAction {
  type: typeof GET_MULTIPLE_MEASUREMENTS;
  payload: MultiMeasurements[];
}

export interface GetLastMeasurementAction {
  type: typeof GET_LAST_MEASUREMENT;
  payload: {
    measurement :Measurement
    metricType: string;
  };
}

export type MetricsActionTypes =
  | GetMetricsAction
  | GetMeasurementsAction
  | MetricsErrorAction
  | GetMultipleMeasurementsAction
  | GetLastMeasurementAction;
