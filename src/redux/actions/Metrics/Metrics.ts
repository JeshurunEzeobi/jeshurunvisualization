//metric actions
import {
  METRICS_ERROR,
  GET_METRICS,
  GET_MEASUREMENTS,
  GET_LAST_MEASUREMENT,
  GET_MULTIPLE_MEASUREMENTS,
} from '../../../constants/actionTypes';
import { MetricsActionTypes, Measurement, MultiMeasurements } from './types';
import { ApolloError } from '@apollo/client';

export const getMetrics = (metrics: string[]): MetricsActionTypes => {
  return {
    type: GET_METRICS,
    payload: metrics,
  };
};

export const getMeasurements = (measurements: Measurement[], metricType: string): MetricsActionTypes => {
  return {
    type: GET_MEASUREMENTS,
    payload: {
      measurements,
      metricType,
    },
  };
};

export const getMultiMeasurements = (multiMeasurements: MultiMeasurements[]): MetricsActionTypes => {
  return {
    type: GET_MULTIPLE_MEASUREMENTS,
    payload: multiMeasurements,
  };
};

export const getMetricError = (error: ApolloError): MetricsActionTypes => {
  return {
    type: METRICS_ERROR,
    payload: error,
  };
};

export const getLastMeasurement = (measurement: Measurement, metricType: string): MetricsActionTypes => {
  return {
    type: GET_LAST_MEASUREMENT,
    payload: {
      measurement,
      metricType,
    },
  };
};
