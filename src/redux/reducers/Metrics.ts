import {
  METRICS_ERROR,
  GET_METRICS,
  GET_MEASUREMENTS,
  GET_LAST_MEASUREMENT,
  GET_MULTIPLE_MEASUREMENTS,
} from '../../constants/actionTypes';
import { MetricsState, MetricsActionTypes } from '../actions/Metrics/types';

const initialState: MetricsState = {
  flareTemps: [],
  injValveOpens: [],
  oilTemps: [],
  casingPressures: [],
  tubingPressures: [],
  waterTemps: [],
  multiMeasurements: [],
  metrics: [],
  flareTemp: {},
  injValveOpen: {},
  oilTemp: {},
  casingPressure: {},
  tubingPressure: {},
  waterTemp: {},
  metricErrors: undefined,
};

export default function MetricsReducer(state = initialState, action: MetricsActionTypes): MetricsState {
  switch (action.type) {
    case METRICS_ERROR:
      return {
        ...state,
        metricErrors: action.payload,
      };

    case GET_METRICS:
      return {
        ...state,
        metrics: action.payload,
      };

    case GET_MEASUREMENTS:
      const metricType = action.payload.metricType;
      if (metricType === 'flareTemp') {
        state.flareTemps = action.payload.measurements;
      } else if (metricType === 'injValveOpen') {
        state.injValveOpens = action.payload.measurements;
      } else if (metricType === 'oilTemp') {
        state.oilTemps = action.payload.measurements;
      } else if (metricType === 'casingPressure') {
        state.casingPressures = action.payload.measurements;
      } else if (metricType === 'tubingPressure') {
        state.tubingPressures = action.payload.measurements;
      } else if (metricType === 'waterTemp') {
        state.waterTemps = action.payload.measurements;
      }

      return {
        ...state,
      };

    case GET_LAST_MEASUREMENT:
      const currentType = action.payload.metricType;
      if (currentType === 'flareTemp') {
        state.flareTemp = action.payload.measurement;
      } else if (currentType === 'injValveOpen') {
        state.injValveOpen = action.payload.measurement;
      } else if (currentType === 'oilTemp') {
        state.oilTemp = action.payload.measurement;
      } else if (currentType === 'casingPressure') {
        state.casingPressure = action.payload.measurement;
      } else if (currentType === 'tubingPressure') {
        state.tubingPressure = action.payload.measurement;
      } else if (currentType === 'waterTemp') {
        state.waterTemp = action.payload.measurement;
      }

      return {
        ...state,
      };

    case GET_MULTIPLE_MEASUREMENTS:
      return {
        ...state,
        multiMeasurements: action.payload,
      };

    default:
      return state;
  }
}
