import { toast } from 'react-toastify';
import { MetricsActionTypes } from '../actions/Metrics/types';
import { all, call, fork, takeEvery} from 'redux-saga/effects';
import { METRICS_ERROR } from '../../constants/actionTypes';
import { PayloadAction } from 'redux-starter-kit';


function* apiMetricError(action: PayloadAction<MetricsActionTypes>) {
    yield call(toast.error, `Error Received: ${action.payload}`);
  }


export function* metricsErrorQuery() {
    yield takeEvery(METRICS_ERROR, apiMetricError);
}

export default function* rootSaga() {
    yield all([
        fork(metricsErrorQuery),
    ])
}
