import { reducer as weatherReducer } from '../reducers/Weather';
import MetricsReducer from '../reducers/Metrics';

export default {
  weather: weatherReducer,
  metrics: MetricsReducer,
};
