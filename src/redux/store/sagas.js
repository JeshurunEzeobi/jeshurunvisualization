import { all } from 'redux-saga/effects';
import weatherSaga from '../sagas/Weather';
import MetricsSaga from '../sagas/Metrics';

export default function* rootSaga() {
  yield all([
    weatherSaga(),
    MetricsSaga(),
  ]);
}
