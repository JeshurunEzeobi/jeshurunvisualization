import React from 'react';
import createStore from './redux/store';
import { Provider } from 'react-redux';
import { ToastContainer } from 'react-toastify';
import CssBaseline from '@material-ui/core/CssBaseline';
import 'react-toastify/dist/ReactToastify.css';
import Header from './components/Header';
import Wrapper from './components/Wrapper';
import NowWhat from './components/NowWhat';
import { MuiThemeProvider } from '@material-ui/core/styles';
import { theme } from './Styles/MaterialUITheme';
import Metrics from './Features/Metrics/Metrics';
import { ApolloClient, InMemoryCache, ApolloProvider} from "@apollo/client";


const client = new ApolloClient({
  uri: 'https://react.eogresources.com/graphql',
  cache: new InMemoryCache()
});

const store = createStore();


const App = () => (
  <ApolloProvider client={client}>
  <MuiThemeProvider theme={theme}>
    <CssBaseline />
    <Provider store={store}>
      <Wrapper>
        <Header />
        <Metrics/>
        <NowWhat />
        <ToastContainer />
      </Wrapper>
    </Provider>
  </MuiThemeProvider>
  </ApolloProvider>
);

export default App;
